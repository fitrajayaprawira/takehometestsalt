//
//  ViewController.swift
//  SaltTechnicalTest
//
//  Created by Prawira Hadi Fitrajaya on 16/11/22.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    private lazy var viewModel: ReqResViewModel = {
        let viewModel = ReqResViewModel(service: ReqResService())
        
        viewModel.successLogin = { [weak self] response in
            DispatchQueue.main.async {
                if response.token != "QpwL5tke4Pnpja7X4" {
                    let alert = UIAlertController(title: "Login Failed", message: "email or password incorrect", preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    self?.present(alert, animated: true, completion: nil)
                } else {
                    let vc = ProfileDashboardViewController()
                    vc.modalPresentationStyle = .fullScreen
                    self?.present(vc, animated: true)
                }
            }
        }
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passwordTextField.isSecureTextEntry = true
        self.loginButton.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
    }

    @objc func loginAction() {
        viewModel.postLogin(email: self.emailTextField.text ?? "", password: self.passwordTextField.text ?? "")
        
    }
    
    @IBAction func emailDone(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    @IBAction func passwordDone(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    
}

