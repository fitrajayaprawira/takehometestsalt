//
//  ProfileDashboardViewController.swift
//  SaltTechnicalTest
//
//  Created by Prawira Hadi Fitrajaya on 16/11/22.
//

import UIKit

class ProfileDashboardViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    private lazy var viewModel: ReqResViewModel = {
        let viewModel = ReqResViewModel(service: ReqResService())

        viewModel.successGetUserInfo = updateUI        

        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getUserInfo(userId: "4")
        
    }
    
    private func updateUI(_ userInfo: SingleUserResponse?) {
        DispatchQueue.main.async {
            self.firstNameLabel.text = "First Name: " + (userInfo?.data?.firstName)!
            self.lastNameLabel.text = "Last Name: " + (userInfo?.data?.lastName)!
            self.emailLabel.text = "Email: " + (userInfo?.data?.email)!
            self.profileImageView.imageFromServerURL(userInfo?.data?.avatar ?? "")
        }
    }
    
    @IBAction func dismissButton(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
}


