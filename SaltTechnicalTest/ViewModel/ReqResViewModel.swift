//
//  ReqResViewModel.swift
//  SaltTechnicalTest
//
//  Created by Prawira Hadi Fitrajaya on 16/11/22.
//

import Foundation

public final class ReqResViewModel {
    
    var service : ReqResProtocol?
    var loginModel : LoginSuccessful?
    
    var userInfoModel : SingleUserResponse? {
        didSet {
            successGetUserInfo?(userInfoModel!)
        }
    }
    
    var successLogin : ((_ response : LoginSuccessful) -> Void)?
    var successGetUserInfo : ((_ response : SingleUserResponse) -> Void)?
    
    var failedGetResponse : ((String) -> Void)?
    
    init(service: ReqResProtocol) {
        self.service = service
    }
    
    func postLogin(email: String, password: String){
        service?.loginUser(email: email, password: password, responseType: LoginSuccessful.self, completion: { [weak self] result in
            switch result {
            case .success(let res):
                self?.successLogin?(res)
                self?.loginModel = res
            case .failure(let error):
                self?.failedGetResponse?(error.localizedDescription)
            }
            
        })
    }
    
    func getUserInfo(userId: String){
        service?.singleUserInfo(userId: userId, responseType: SingleUserResponse.self, completion: { [weak self] result in
            switch result {
            case .success(let res):
                self?.successGetUserInfo?(res)
                self?.userInfoModel = res
            case .failure(let error):
                self?.failedGetResponse?(error.localizedDescription)
            }
            
        })
    }
}
