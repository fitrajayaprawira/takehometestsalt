//
//  UIImageViewExt.swift
//  SaltTechnicalTest
//
//  Created by Prawira Hadi Fitrajaya on 16/11/22.
//

import Foundation
import UIKit

extension UIImageView {
    
    func imageFromServerURL(_ URLString: String) {
        
        self.image = nil
        
        if let url = URL(string: URLString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            
                            self.image = downloadedImage
                        }
                    }
                }
            }).resume()
        }
    }
}
