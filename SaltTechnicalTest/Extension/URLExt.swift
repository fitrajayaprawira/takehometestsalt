//
//  URLExt.swift
//  SaltTechnicalTest
//
//  Created by Prawira Hadi Fitrajaya on 16/11/22.
//

import Foundation

extension URL {
    static func construct(_ endpoint: String, queries: [String : String] = [:]) -> URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.path = "/" + endpoint
        components.queryItems = queries.isEmpty ? nil : queries
            .map({ URLQueryItem(name: $0.key, value: $0.value) })
        return components.url
    }
}
