//
//  ReqResService.swift
//  SaltTechnicalTest
//
//  Created by Prawira Hadi Fitrajaya on 16/11/22.
//

import Foundation

public protocol ReqResProtocol: AnyObject {
    func loginUser<T: Decodable>(email: String, password: String, responseType: T.Type, completion: @escaping (APIResult<T>) -> Void)
    
    func singleUserInfo<T: Decodable>(userId: String, responseType: T.Type, completion: @escaping (APIResult<T>) -> Void)
}

public class ReqResService : ReqResProtocol {
    
    public static let shared = ReqResService()
    
    public func loginUser<T: Decodable>(email: String, password: String, responseType: T.Type, completion: @escaping (APIResult<T>) -> Void) {
        let parameters = [
            "email": email,
            "password": password]
        
        NetworkingService.shared.request("POST", "reqres.in/api/login", parameters: parameters , responseType: responseType) { result in
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func singleUserInfo<T: Decodable>(userId: String, responseType: T.Type, completion: @escaping (APIResult<T>) -> Void) {
        NetworkingService.shared.request("GET", "reqres.in/api/users/\(userId)", responseType: responseType) { result in
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
