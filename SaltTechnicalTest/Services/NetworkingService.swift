//
//  NetworkingService.swift
//  SaltTechnicalTest
//
//  Created by Prawira Hadi Fitrajaya on 15/11/22.
//

import Foundation

public typealias APIResult<T: Decodable> = Result<T, Error>

public protocol NetworkingServiceProtocol: AnyObject {
    func request<T: Decodable>(_  method: String, _ endpoint: String, parameters: [String : String], responseType: T.Type, completion: @escaping (_ result: Result<T, Error>) -> Void)
}

public final class NetworkingService: NetworkingServiceProtocol {
    
    public static let shared = NetworkingService()

    private func constructRequest(_ method: String, _ endpoint: String, parameters: [String : String] = [:]) -> URLRequest {
        
        let defaultHeaders: [String: String] = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        guard let url = URL.construct(endpoint) else {
            fatalError("Invalid URL, please check `NetworkingEndpoint`")
        }

        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = defaultHeaders
        request.httpMethod = method.uppercased()
        
        if method != "GET" {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } else {
            request.url = URL.construct(endpoint, queries: parameters)
        }
        
        return request
    }

    public func request<T: Decodable>(_ method: String = "GET", _ endpoint: String, parameters: [String : String] = [:], responseType: T.Type, completion: @escaping (_ result: Result<T, Error>) -> Void) {
        
        let request = constructRequest(method, endpoint, parameters: parameters)
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async {
                
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                let url = request.url?.absoluteString ?? ""
                var logOutput = "HTTP_REQUEST: \(method) \(url) \n"
                if method != "GET" {
                    logOutput += "BODY: \(parameters.debugDescription) \n"
                }
                if let responseJSON = String(data: data, encoding: .utf8) {
                    logOutput += "JSON: \(responseJSON)"
                }
                print(logOutput)
                
                do {
                    let result = try JSONDecoder().decode(responseType, from: data)
                    completion(.success(result))
                } catch let error {
                    completion(.failure(error))
                }
            }
        })
        task.resume()
    }
}
