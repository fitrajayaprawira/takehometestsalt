//
//  ReqResModel.swift
//  SaltTechnicalTest
//
//  Created by Prawira Hadi Fitrajaya on 15/11/22.
//

import Foundation

struct LoginSuccessful: Codable {
    let token: String?
}

struct SingleUserResponse: Codable {
    let data: SingleUserModel?
}

struct SingleUserModel: Codable {
    let id: Int?
    let email, firstName, lastName: String?
    let avatar: String?

    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case avatar
    }
}
